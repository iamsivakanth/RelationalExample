package com.training.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class OperationsTest {

	@Test // @Test is an annotation used to make a test method
	public void equalsTest() {
		Operations operations = new Operations();
		boolean actual = operations.equals();
		Assertions.assertFalse(actual);
	}
}